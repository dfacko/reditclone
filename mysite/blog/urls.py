from . import views
from django.urls import path,include

#app_name='blog'

urlpatterns=[

    path('',views.PostList.as_view(),name='home'),
    path('<slug:slug>/',views.post_detail,name='post_detail'),
    path('upvote', views.upvote,name='upvote'),
    path('downvote', views.downvote,name='downvote'),
    path('reply',views.replytocomment,name='reply'),
    path('edit',views.editComment,name='edit'),
    path('save',views.save,name='save'),
    path('nArticle',views.nArticle,name='nArticle'),
]
