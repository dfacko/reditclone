from django.db import models
from vote.models import VoteModel
from django.contrib.auth.models import User

# Create your models here.


Status = ((0,"Draft",),(1,"Publish"))

class Post(VoteModel,models.Model):
    title=models.CharField(max_length=200,unique=True)
    slug=models.SlugField(max_length=200,unique=True)
    author=models.ForeignKey(User,on_delete=models.CASCADE,related_name='blog_posts')
    updated_on=models.DateTimeField(auto_now=True)
    content=models.TextField()
    created_on=models.DateTimeField(auto_now_add=True)
    status=models.IntegerField(choices=Status,default=0)

    class Meta:
        ordering = ['-created_on']

    def __str__(self):
        return self.title


class Comment(VoteModel,models.Model):
    post=models.ForeignKey(Post,on_delete=models.CASCADE,related_name='comments')
    name=models.ForeignKey(User,default=None,on_delete=models.CASCADE)
    email=models.EmailField()
    body=models.TextField()
    created_on=models.DateTimeField(auto_now_add=True)
    active=models.BooleanField(default=False)
    reply=models.ForeignKey('Comment',null=True,related_name="replies",on_delete=models.CASCADE)
    vote = models.IntegerField(default=0)
    parent_id=models.IntegerField(default=0)

    class Meta:
        ordering=['created_on']


    def __str__(self):
        return 'Comment {} by {}'.format(self.body, self.name)