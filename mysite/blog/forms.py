from .models import Comment,Post
from django import forms


class CommentForm(forms.ModelForm):
    body = forms.CharField(label="",widget=forms.Textarea(attrs={'class':'form-control','placeholder':'text goes here','rows':'4','cols':'50'}))
    class Meta:
        model=Comment
        fields=('body',)

class ArticleForm(forms.ModelForm):
    content = forms.CharField(label="",widget=forms.Textarea(attrs={'class':'form-control','placeholder':'text goes here','rows':'20','cols':'50'}))
    title = forms.CharField(label="",widget=forms.Textarea(attrs={'class':'form-control','placeholder':'Title','rows':'4','cols':'20'}))
    slug = forms.SlugField(label="",widget=forms.Textarea())
    class Meta:
        model=Post
        fields=('content','title','slug')
