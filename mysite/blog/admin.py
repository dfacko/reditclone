from django.contrib import admin
from .models import Post, Comment
from django.shortcuts import render

# Register your models here.

class PostAdmin(admin.ModelAdmin):
    list_display = ('title', 'slug', 'status','created_on')
    list_filter = ("status",)
    search_fields = ['title', 'content'] #kriterij prema kojem mozemo pretrazivat postove na admin stranici"""
    prepopulated_fields = {'slug': ('title',)}
    actions=['publish_article']


    def publish_article(self,request,queryset):
        queryset.update(status=1)

  
admin.site.register(Post, PostAdmin)

class CommentAdmin(admin.ModelAdmin):
    list_display=('name','body','post','created_on','active','vote','parent_id','id')
    list_filter=('active','created_on')
    search_fields=('name','email','body')
    actions=['approve_comments','edit_comment']

    def approve_comments(self,request,queryset):
        queryset.update(active=True)
    def edit_comment(self,request,queryset):
        return 
admin.site.register(Comment,CommentAdmin)