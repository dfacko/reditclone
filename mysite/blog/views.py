from django.shortcuts import render,redirect
from .models import Post,Comment
from django.views import generic
from django.shortcuts import render,get_object_or_404
from .forms import CommentForm,ArticleForm

# Create your views here.

class PostList(generic.ListView):
    queryset=Post.objects.filter(status=1).order_by('-created_on')
    template_name='index.html'

#class PostDetail(generic.DetailView):
#    model=Post
#    template_name='post_detail.html'


def post_detail(request, slug):
    template_name = "post_detail.html"
    post = get_object_or_404(Post, slug=slug)
    comments = post.comments.filter(active=True,).order_by("-created_on")
    new_comment = None
    # Comment posted
    if request.method == "POST":
        comment_form = CommentForm(data=request.POST)
        if comment_form.is_valid():
            #need to insert active users name here
            # Create Comment object but don't save to database yet
            new_comment = comment_form.save(commit=False)
            new_comment.name=request.user
            # Assign the current post to the comment
            new_comment.post = post
            if new_comment.name.username=="admin":
                new_comment.active=True
            # Save the comment to the database
            new_comment.save()

            return redirect('/'+ slug)
    else:
        comment_form = CommentForm()

    return render(
        request,
        template_name,
        {
            "post": post,
            "comments": comments,
            "new_comment": new_comment,
            "comment_form": comment_form,
        },
    )

def upvote(request):
    comment_id=None
    comment_id=request.POST.get('comment_id')
    comment=get_object_or_404(Comment,id=comment_id)
    userId=request.user.id
    if request.user.is_authenticated:
        if comment.votes.exists(userId):
            comment.votes.delete(userId)
    post=comment.post.slug
    comment.votes.up(userId)
    #comment.vote+=1
    comment.save()
    
    return redirect('/'+post)


def downvote(request):
    comment_id=None
    comment_id=request.POST.get('comment_id')
    comment=get_object_or_404(Comment,id=comment_id)
    userId=request.user.id
    if request.user.is_authenticated:
        if comment.votes.exists(userId):
            comment.votes.delete(userId)
        #comment.save()
    post=comment.post.slug
    comment.votes.down(userId)
    #comment.vote-=1
    comment.save()
    
    return redirect('/'+post)

def replytocomment(request):
    comment_form = CommentForm(data=request.POST)
    comment_id=0
    if comment_form.is_valid():
        comment_id=request.POST.get('comment_id')
        comment=get_object_or_404(Comment,id=comment_id)
        new_comment = comment_form.save(commit=False)
        new_comment.name=request.user
        new_comment.reply=comment
        new_comment.post=comment.post
        new_comment.parent_id=comment_id
        if new_comment.name.username=="admin":
            new_comment.active=True
        new_comment.save()

    return redirect('/'+ comment.post.slug)


#def leavecomment(request,slug):
#    comment_form = CommentForm(data=request.POST)
#    post = get_object_or_404(Post, slug=slug)
#    if comment_form.is_valid():
#        new_comment = comment_form.save(commit=False)
#        new_comment.name=request.user
#        new_comment.post = post
#        new_comment.save()#
#
#    return redirect('/'+ post.slug)

def editComment(request):
    
    template_name='edit.html'
    comment_id=request.POST.get('comment_id')
    post_id=request.POST.get('post_id')
    if comment_id:
        comment=get_object_or_404(Comment,id=comment_id)
        commentText=comment.body
        #comment_form=CommentForm(data=request.POST)

        return render(
            request,
            template_name,
            {
                "comment_id" : comment_id,
                #"commentText" :comment_form
                "commentText" :commentText,
            },
        )
    if post_id:
        #post_form=ArticleForm(data=request.POST)
        post=get_object_or_404(Post,id=post_id)
        postText=post.content
        return render(
            request,
            template_name,
            {
                "post_id":post_id,
                #"postText":post_form
                "postText":postText,

            }
        )

def save(request):

    #comment_form=CommentForm(data=request.POST)
    comment_id=request.POST.get('comment_id')
    post_id=request.POST.get('post_id')
    if comment_id:
        comment_text=request.POST.get('text')
        comment=get_object_or_404(Comment,id=comment_id)
        slug=comment.post.slug
        if comment_text=='':
            comment.delete()
            return redirect('/'+slug)
        comment.body=comment_text
        if request.user.username!="admin":
            comment.active=False
        comment.save()
        
        return redirect('/'+slug)
    if post_id:
        postText=request.POST.get('text')
        post=get_object_or_404(Post,id=post_id)
        if postText=='':
            post.delete()
            return redirect('home')
        post.content=postText
        if request.user.username!="admin":
            post.status=0
        post.save()

        return redirect ('home')


def nArticle (request):
    template_name = "nArticle.html"
    Article_Form=ArticleForm()
    if request.method=="POST":
        if Article_Form.is_valid:
            Article_Form=ArticleForm(data=request.POST)
            new_article=Article_Form.save(commit=False)
            new_article.author=request.user
            if new_article.author.username=="admin":
                new_article.status=1
            new_article.save()
            return redirect('home')
    else:
        return render(
            request,
            template_name,
        {
            "ArticleForm": ArticleForm,
        }
        )